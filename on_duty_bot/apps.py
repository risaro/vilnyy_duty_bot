from django.apps import AppConfig


class OnDutyBotConfig(AppConfig):
    name = 'on_duty_bot'
