import datetime
import re
import telebot
import logging
from ondutybot.settings import API_TOKEN
from .models import *
from calendar import monthrange
from utils.telegramcalendar import create_calendar
from dateutil.relativedelta import relativedelta
from django.db.models.functions import ExtractDay, ExtractMonth, ExtractYear
from django.db.models import Count
from random import choice

bot = telebot.TeleBot(API_TOKEN)
telebot.logger.setLevel(logging.INFO)
current_shown_dates = {}


def update_calendar(call, date):
    now = date
    tg_group = UserGroup.objects.get(telegram_group_id=call.message.chat.id)
    taken_dates = list(DutyDate.objects.annotate(
        month=ExtractMonth('date'),
        day=ExtractDay('date')).values_list('day', flat=True).filter(
        month=now.month,
        group=tg_group,
        telegram_user__isnull=False))
    new_calendar = create_calendar(now.year, now.month, taken_dates)
    try:
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=call.message.text,
                              reply_markup=new_calendar)
    except telebot.apihelper.ApiException:
        print('Exception')


def get_schedule_str(group_id: int, date: datetime):
    result_msg = 'Графік чергування на {}/{}\n'.format(date.month, date.year)
    dates = DutyDate.objects.annotate(
        day=ExtractDay('date'),
        month=ExtractMonth('date')
    ).filter(
        group__telegram_group_id=group_id,
        month=date.month
    ).order_by('day')
    for day in dates:
        user = day.telegram_user
        mention = create_mention(user)
        result_msg += '{} — {}\n'.format(day.date.strftime('%d/%m'), mention)

    return result_msg


@bot.message_handler(commands=['start'])
def command_start(message):
    tg_user = message.from_user
    new_user, created = TelegramUser.objects.get_or_create(telegram_id=tg_user.id)
    if created:
        new_user.from_tg(tg_user)
    bot.send_message(message.chat.id, 'Привіт! Щоб розпочати роботу, додай мене до свого чату.'
                                      ' Учасники можуть приєднуватися за допомогою команди /join@vilnyy_duty_bot'
                                      '/nСписок команд: /help')


@bot.message_handler(commands=['help'])
def command_help(message):
    help_str = '/help - список команд' + \
               '\n/join - доєднатися до групи' + \
               '\n/begin - почати бронювання на наступний місяць' + \
               '\n/sotnyk <user> - призначити сотника, який чергуватиме решту днів' + \
               '\n/set <користувач> <дд/мм/рррр> - призначити чергового на конкретний день' + \
               '\n/kick <користувач> - видалити користувача з групи' + \
               '\n/finish - закінчити бронювання і визначити решту чергувань випадковим чином' + \
               '\n/list - переглянути учасників групи' + \
               '\n/current - переглянути графік на поточний місяць' + \
               '\n/next - переглянути графік на наступний місяць' + \
               '\n/who - хто чергує сьогодні'
    bot.send_message(message.chat.id, help_str)


def is_admin(chat_id, user_id):
    chat_member = bot.get_chat_member(chat_id, user_id)
    if chat_member.status in {'creator', 'administrator'} or chat_member.user.id == 295288996:
        return True
    return False


@bot.message_handler(commands=['join'])
def command_join(message):
    tg_user = message.from_user
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Ця команда призначена лише для використання у групах')
    else:
        user, created = TelegramUser.objects.get_or_create(telegram_id=tg_user.id)
        if created:
            user.from_tg(tg_user)
        new_group, _created = UserGroup.objects.get_or_create(telegram_group_id=message.chat.id)
        new_group.members.add(user)
        mention = create_mention(user)
        result_msg = '{} тепер теж чергуватиме'.format(mention)

        bot.send_message(message.chat.id, result_msg, parse_mode="HTML", disable_notification=True)


@bot.message_handler(commands=['kick'])
def command_kick(message):
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Ця команда призначена лише для використання у групах')
    else:
        if not is_admin(message.chat.id, message.from_user.id):
            bot.send_message(message.chat.id, 'Команду може використовувати тільки адміністратор')
            return

        group, _created = UserGroup.objects.get_or_create(telegram_group_id=message.chat.id)
        if _created:
            bot.send_message(message.chat.id, 'До даної групи ще ніхто не приєднався')
            group.delete()
        else:
            user_id_search = re.search('(?<=id=)\d+|(?<=@)\w+\S', message.html_text)
            if user_id_search:
                user_id = user_id_search.group(0)
                if user_id.isdigit():
                    user, created = TelegramUser.objects.get_or_create(telegram_id=int(user_id))
                else:
                    user, created = TelegramUser.objects.get_or_create(telegram_username=user_id)

                if created:
                    bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')
                    user.delete()
                else:
                    group.members.remove(user)
                    group.save()
                    bot.send_message(message.chat.id, 'Користувача {} видалено з групи'.format(create_mention(user)))
            else:
                bot.send_message(message.chat.id,
                                 'Користувача вказано невірно. Для роботи команди "тегніть" необхідного '
                                 'користувача у повідомленні')

# @bot.message_handler(commands=['leave'])
# def command_leave(message):
#     if message.chat.type == 'private':
#         bot.send_message(message.chat.id, 'Ця команда призначена лише для використання у групах')
#     else:
#         tg_user = message.from_user
#         user, created = TelegramUser.objects.get_or_create(telegram_id=tg_user.id)
#         if created:
#             bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')
#             user.delete()
#         else:
#             group, _created = UserGroup.objects.get_or_create(telegram_group_id=message.chat.id)
#             if _created:
#                 bot.send_message(message.chat.id, 'До даної групи ще ніхто не приєднався')
#                 group.delete()
#             else:
#                 group.members.remove(user)
#                 group.save()
#                 mention = create_mention(user)
#                 bot.send_message(message.chat.id, '{} більше не чергуватиме'.format(mention),
#                                  disable_notification=True)


@bot.message_handler(commands=['set'])
def command_set(message):
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Ця команда призначена лише для використання у групах')
    else:
        if not is_admin(message.chat.id, message.from_user.id):
            bot.send_message(message.chat.id, 'Команду може використовувати тільки адміністратор')
            return

        group, _created = UserGroup.objects.get_or_create(telegram_group_id=message.chat.id)
        if _created:
            bot.send_message(message.chat.id, 'До даної групи ще ніхто не приєднався')
            group.delete()
        else:
            user_id_search = re.search('(?<=id=)\d+|(?<=@)\w+\S', message.html_text)
            if user_id_search:
                user_id = user_id_search.group(0)
                if user_id.isdigit():
                    user, created = TelegramUser.objects.get_or_create(telegram_id=int(user_id))
                else:
                    user, created = TelegramUser.objects.get_or_create(telegram_username=user_id)

                if created:
                    bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')
                    user.delete()
                else:
                    date_search = re.search('\d{2}\/\d{2}\/\d{4}', message.html_text)
                    if date_search:
                        date_str = date_search.group(0)
                        date_str_split = date_str.split('/')
                        _day = date_str_split[0]
                        _month = date_str_split[1]
                        _year = date_str_split[2]

                        duty_date, created = DutyDate.objects.annotate(
                            day=ExtractDay('date'),
                            month=ExtractMonth('date'),
                            year=ExtractYear('date')
                        ).get_or_create(
                            day=_day,
                            month=_month,
                            year=_year,
                            group=group
                        )
                        if created:
                            bot.send_message(message.chat.id, 'Цю дату ще не затверджено.'
                                                              'Примусове призначення можливе '
                                                              'лише після закінчення бронювань')
                        else:
                            if duty_date.telegram_user == user:
                                bot.send_message(message.chat.id, 'Користувач і так чергуватиме цього дня')
                            else:
                                duty_date.telegram_user = user
                                duty_date.save()
                                mention = create_mention(user)
                                bot.send_message(message.chat.id, '{} тепер чергуватиме {}'.format(mention, date_str),
                                                 parse_mode='HTML')

                    else:
                        bot.send_message(message.chat.id, 'Дату вказакно невірно. Необхідний формат: дд/мм/рррр.')

            else:
                bot.send_message(message.chat.id,
                                 'Користувача вказано невірно. Для роботи команди "тегніть" необхідного '
                                 'користувача у повідомленні')


@bot.message_handler(commands=['begin'])
def command_begin(message):
    if not is_admin(message.chat.id, message.from_user.id):
        bot.send_message(message.chat.id, 'Команду може використовувати тільки адміністратор')
        return
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Ця команда призначена лише для використання у групах')
    else:
        now = datetime.datetime.now() + relativedelta(months=1)
        chat_id = message.chat.id

        date = (now.year, now.month)
        current_shown_dates[chat_id] = date

        tg_group = UserGroup.objects.get(telegram_group_id=message.chat.id)
        if not tg_group.head:
            bot.send_message(message.chat.id, 'Спочатку необхідно вказати сотника. Команда /sontyk <користувач>')
            return

        sample_day = DutyDate.objects.annotate(
            month=ExtractMonth('date')
        ).filter(
            group=tg_group,
            month=now.month
        ).first()

        if sample_day and not sample_day.is_open:
            bot.send_message(message.chat.id, 'Графік чергування на наступний місяць вже затверджено.')
            return

        taken_dates = list(DutyDate.objects.annotate(
            month=ExtractMonth('date'),
            day=ExtractDay('date')).values_list('day', flat=True).filter(
            month=now.month,
            group=tg_group,
            telegram_user__isnull=False))

        markup = create_calendar(now.year, now.month, taken_dates)

        msg = bot.send_message(message.chat.id, 'Оберіть два дні для чергування', reply_markup=markup)
        try:
            bot.pin_chat_message(message.chat.id, msg.message_id, disable_notification=True)
        except:
            pass


@bot.message_handler(commands=['sotnyk'])
def command_sotnyk(message):
    if not is_admin(message.chat.id, message.from_user.id):
        bot.send_message(message.chat.id, 'Команду може використовувати тільки адміністратор')
        return
    text = message.html_text.split('bot')[1].strip()
    try:
        group = UserGroup.objects.get(telegram_group_id=message.chat.id)
    except models.ObjectDoesNotExist:
        bot.send_message(message.chat.id, 'Жоден учасник ще не доєднався до групи')
        return

    if '@' in text:
        # Removing first character -- '@'
        user = TelegramUser.objects.get(telegram_username=text[1:])
    else:
        user_id_search = re.search('\d+', text, re.IGNORECASE)
        if user_id_search:
            user_id = int(user_id_search.group(0))
            try:
                user = TelegramUser.objects.get(telegram_id=user_id)
            except models.ObjectDoesNotExist:
                bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')
                return
        else:
            bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')
            return

    if user in group.members.all():
        group.head = user
        group.save()
        mention = create_mention(user)
        bot.send_message(message.chat.id, 'Користувача {} чергуватиме решту днів.'.format(mention),
                         disable_notification=True)
    else:
        bot.send_message(message.chat.id, 'Користувач ще не приєднався до групи')


@bot.message_handler(commands=['finish'])
def command_finish(message):
    if not is_admin(message.chat.id, message.from_user.id):
        bot.send_message(message.chat.id, 'Команду може використовувати тільки адміністратор')
        return
    now = datetime.datetime.now() + relativedelta(months=1)
    group = UserGroup.objects.filter(telegram_group_id=message.chat.id).first()

    users_to_set = TelegramUser.objects.annotate(
        nduty=Count('dutydate')
    ).filter(
        nduty__lt=2,
        usergroup=group
    )

    dates_to_set = []

    num_days = monthrange(now.year, now.month)[1]
    days = [datetime.date(now.year, now.month, day) for day in range(1, num_days + 1)]
    for day in days:
        new_date, created = DutyDate.objects.get_or_create(
            group=group,
            date=day
        )
        if created or new_date.telegram_user is None:
            dates_to_set.append(new_date)
        elif not new_date.is_open:
            bot.send_message(message.chat.id, 'Бронювання дат на цей місяць завершено')
            return

    for user in users_to_set:
        duties_count = DutyDate.objects.filter(
            group=group,
            telegram_user=user
        ).count()

        for i in range(2 - duties_count):
            random_day = choice(dates_to_set)
            random_day.telegram_user = user
            random_day.save()
            dates_to_set.remove(random_day)

    # Sotnyk part. Will iterate only through days left, taken days will be removed in previous part

    for day in dates_to_set:
        day.telegram_user = group.head
        day.save()

    DutyDate.objects.annotate(
        month=ExtractMonth('date')
    ).filter(
        group=group,
        month=now.month
    ).update(is_open=False)

    result_msg = get_schedule_str(message.chat.id, now)

    msg = bot.send_message(message.chat.id, result_msg, parse_mode='HTML', disable_notification=True)
    try:
        bot.pin_chat_message(message.chat.id, msg.message_id, disable_notification=True)
    except:
        pass


@bot.message_handler(commands=['who'])
def command_who(message):
    now = datetime.datetime.now()

    user = DutyDate.objects.annotate(
        day=ExtractDay('date'),
        month=ExtractMonth('date'),
        year=ExtractYear('date')
    ).filter(
        group__telegram_group_id=message.chat.id,
        day=now.day,
        month=now.month,
        year=now.year
    ).first().telegram_user

    mention = create_mention(user)
    msg = 'Сьогодні чергує {}.'.format(mention)
    bot.send_message(message.chat.id, msg, parse_mode='HTML')


@bot.message_handler(commands=['list'])
def command_list(message):
    users = TelegramUser.objects.filter(
        usergroup__telegram_group_id=message.chat.id)

    result_msg = 'Користувачі, що приєдналися:\n'
    for i, user in enumerate(users):
        mention = create_mention(user)
        result_msg += '{}. {}\n'.format(i + 1, mention)
    bot.send_message(message.chat.id, result_msg, parse_mode='HTML', disable_notification=True)


@bot.message_handler(commands=['current'])
def command_current(message):
    now = datetime.datetime.now()
    dates = DutyDate.objects.annotate(
        month=ExtractMonth('date'),
        day=ExtractDay('date')
    ).filter(
        month=now.month,
        group__telegram_group_id=message.chat.id
    ).order_by('day')
    if not dates.first() or dates.first().is_open:
        bot.send_message(message.chat.id, 'Графік на поточний місяць ще не затверджено.')
    else:
        result_msg = get_schedule_str(message.chat.id, now)
        msg = bot.send_message(message.chat.id, result_msg, parse_mode='HTML', disable_notification=True)
        try:
            bot.pin_chat_message(message.chat.id, msg.message_id, disable_notification=True)
        except:
            pass


@bot.message_handler(commands=['next'])
def command_next(message):
    now = datetime.datetime.now() + relativedelta(months=1)
    dates = DutyDate.objects.annotate(
        month=ExtractMonth('date'),
        day=ExtractDay('date')
    ).filter(
        month=now.month,
        group__telegram_group_id=message.chat.id
    ).order_by('day')
    if not dates.first() or dates.first().is_open:
        bot.send_message(message.chat.id, 'Графік на наступний місяць ще не затверджено.')
    else:
        result_msg = get_schedule_str(message.chat.id, now)
        msg = bot.send_message(message.chat.id, result_msg, parse_mode='HTML', disable_notification=True)
        try:
            bot.pin_chat_message(message.chat.id, msg.message_id, disable_notification=True)
        except:
            pass


def create_mention(user):
    if user.telegram_username:
        mention = '@{}'.format(user.telegram_username)
    else:
        # mention = '[{}](tg://user?id={})'.format(user.first_name, user.telegram_id).replace("_", "\\_")
        mention = '<a href="tg://user?id={}">{}</a>'.format(user.telegram_id, user.first_name)
    return mention


@bot.callback_query_handler(func=lambda call: 'DAY' in call.data[0:13])
def handle_day_query(call):
    chat_id = call.message.chat.id
    saved_date = current_shown_dates.get(chat_id)
    last_sep = call.data.rfind(';') + 1

    if saved_date is not None:
        tg_user = TelegramUser.objects.get(telegram_id=call.from_user.id)
        day = call.data[last_sep:].replace('*', '')
        date = datetime.datetime(int(saved_date[0]), int(saved_date[1]), int(day), 0, 0, 0)
        group = UserGroup.objects.get(telegram_group_id=chat_id)
        db_date, created = DutyDate.objects.get_or_create(date=date, group=group)
        if not created and db_date.telegram_user:
            if group.head == tg_user:
                bot.answer_callback_query(call.id, 'Сотник не вибирає дні для чергувань')
                return
            if db_date.telegram_user == tg_user:
                db_date.delete()
                answer_str = 'Ви успішно скасували бронювання {}'.format(date.strftime('%d.%m'))
                bot.answer_callback_query(call.id, answer_str)
                update_calendar(call, date)
            elif not db_date.is_open:
                bot.answer_callback_query(call.id, 'Бронювання дат на цей місяць завершено')
            else:
                answer_str = '{} вже зайнято користувачем {}'.format(
                    date.strftime('%d.%m'),
                    db_date.telegram_user.first_name)
                bot.answer_callback_query(call.id, text=answer_str)
        else:
            if group.head == tg_user:
                bot.answer_callback_query(call.id, 'Сотник не вибирає дні для чергувань')
                return
            days_count = DutyDate.objects.filter(telegram_user=tg_user, group_id=chat_id).count()
            if days_count >= 2:
                bot.answer_callback_query(call.id, 'Ви вже забронювали 2 дні')
            else:
                db_date.telegram_user = tg_user
                db_date.save()
                answer_str = '{} успішно заброньовано за Вами!'.format(date.strftime('%d.%m'))
                bot.answer_callback_query(call.id, answer_str)
                update_calendar(call, date)

    else:
        # add your reaction for shown an error
        pass


@bot.callback_query_handler(func=lambda call: 'IGNORE' in call.data)
def ignore(call):
    bot.answer_callback_query(call.id, text='')
