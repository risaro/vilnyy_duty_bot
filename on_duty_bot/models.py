from django.db import models


# Create your models here.
class TelegramUser(models.Model):
    telegram_id = models.IntegerField()
    telegram_username = models.TextField(max_length=255, default=None, blank=True, null=True)
    first_name = models.TextField(max_length=255, default=None, blank=True, null=True)
    last_name = models.TextField(max_length=255, default=None, blank=True, null=True)
    has_blocked = models.BooleanField(default=False)

    def from_tg(self, tg_user):
        self.telegram_id = tg_user.id
        self.first_name = tg_user.first_name
        self.last_name = tg_user.last_name
        self.telegram_username = tg_user.username
        self.save()


class UserGroup(models.Model):
    members = models.ManyToManyField(TelegramUser)
    telegram_group_id = models.IntegerField(default=-1, blank=True, null=True)
    head = models.ForeignKey(TelegramUser, related_name='head', on_delete=models.SET_NULL, null=True)


class DutyDate(models.Model):
    date = models.DateField(default=None, blank=True, null=True)
    telegram_user = models.ForeignKey(TelegramUser, on_delete=models.SET_NULL, null=True, default=None)
    group = models.ForeignKey(UserGroup, on_delete=models.CASCADE, null=True)
    mark = models.FloatField(max_length=255, default=0)
    is_open = models.BooleanField(default=True)
