from django.http import HttpResponse
from .models import *
from .bot import bot, create_mention, API_TOKEN
import datetime
import telebot
import json
from dateutil.relativedelta import relativedelta
from django.db.models.functions import ExtractDay, ExtractMonth, ExtractYear
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def start(request):
    print("Polling started.")
    bot.remove_webhook()
    bot.polling()
    return HttpResponse('')


def remind(request):
    show_next_str = request.GET.get('next')
    now = datetime.datetime.utcnow()
    finished_days = DutyDate.objects.annotate(
        day=ExtractDay('date'),
        month=ExtractMonth('date'),
        year=ExtractYear('date')
    ).filter(
        day=now.day,
        month=now.month,
        year=now.year,
        is_open=False
    )
    for day in finished_days:
        group = day.group
        mention = create_mention(day.telegram_user)
        msg = 'Нагадую, сьогодні чергує {}.'.format(mention)
        if show_next_str == 'true':
            now += relativedelta(days=1)
            next_day_search = DutyDate.objects.annotate(
                day=ExtractDay('date'),
                month=ExtractMonth('date'),
                year=ExtractYear('date')
            ).filter(
                day=now.day,
                month=now.month,
                year=now.year,
                group=group
            )
            if next_day_search:
                next_day = next_day_search.first()
                mention_next = create_mention(next_day.telegram_user)
                msg += '\nЗавтра чергуватиме {}.'.format(mention_next)
        bot.send_message(group.telegram_group_id, msg, parse_mode='HTML')
    return HttpResponse('Done!')


def set_hook(request):
    bot.remove_webhook()
    bot.set_webhook(url='https://ondutybot.tk/telegram/{}'.format(API_TOKEN))
    return HttpResponse('success')


@csrf_exempt
def telegram(request):
    if request.method == 'POST':
        json_string = json.dumps(json.loads(request.body))
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
    return HttpResponse('')
